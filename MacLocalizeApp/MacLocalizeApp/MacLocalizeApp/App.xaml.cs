﻿using MacLocalizeApp.Resx;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace MacLocalizeApp
{
	public partial class App : Application
	{
		public App ()
		{
			InitializeComponent();
            var ci = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();
            L10n.SetLocale(ci);
            AppResources.Culture = ci;
            MainPage = new MacLocalizeApp.MainPage();
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
