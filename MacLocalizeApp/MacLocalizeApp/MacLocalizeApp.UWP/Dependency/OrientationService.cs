﻿using MacLocalizeApp.Interfaces;
using MacLocalizeApp.UWP.Dependency;
using Windows.Graphics.Display;
using Xamarin.Forms;

[assembly: Xamarin.Forms.Dependency(typeof(OrientationService))]
namespace MacLocalizeApp.UWP.Dependency
{
    public class OrientationService : IOrientationService
        {
            public void Landscape()
            {
                DisplayInformation.AutoRotationPreferences = DisplayOrientations.Landscape;
            }

            public void Portrait()
            {
                DisplayInformation.AutoRotationPreferences = DisplayOrientations.Portrait;
            }
        }
    }