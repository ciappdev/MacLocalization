﻿using Xamarin.Forms;

namespace MacLocalizeApp.Resx
{
    class StaticResources
    {
        public static Color ButtonBackColor
        {
            get
            {
                return Color.FromHex("#1D7139");
            }
        }
        public static Color ButtonBackColorPressed
        {
            get
            {
                return Color.FromHex("#184723");
            }
        }
    }
}
