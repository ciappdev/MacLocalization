﻿

using Foundation;
using MacLocalizeApp.Interfaces;
using MacLocalizeApp.iOS.Dependency;
using UIKit;

[assembly: Xamarin.Forms.Dependency(typeof(OrientationService))]
namespace MacLocalizeApp.iOS.Dependency
{
    public class OrientationService : IOrientationService
    {
        public void Landscape()
        {
            UIDevice.CurrentDevice.SetValueForKey(new NSNumber((int)UIInterfaceOrientation.LandscapeLeft), new NSString("orientation"));
        }

        public void Portrait()
        {
            UIDevice.CurrentDevice.SetValueForKey(new NSNumber((int)UIInterfaceOrientation.Portrait), new NSString("orientation"));
        }
    }
}