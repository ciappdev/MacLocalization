using MacLocalizeApp.UWP.Dependency;
using System.Globalization;
using Xamarin.Forms;

[assembly: Dependency(typeof(Localize))]
namespace MacLocalizeApp.UWP.Dependency
{
    public class Localize : ILocalize
    {
        public CultureInfo GetCurrentCultureInfo()
        {
            return new CultureInfo(
                Windows.System.UserProfile.GlobalizationPreferences.Languages[0].ToString());
        }

        public void SetLocale(CultureInfo ci)
        {
            // Do nothing
        }
    }
}
