﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using Android.Content.PM;
using MacLocalizeApp.Droid.Dependency;
using MacLocalizeApp.Interfaces;

[assembly: Xamarin.Forms.Dependency(typeof(OrientationService))]
namespace MacLocalizeApp.Droid.Dependency
{
    public class OrientationService : IOrientationService
        {
            public void Landscape()
            {
                ((Activity)Forms.Context).RequestedOrientation = ScreenOrientation.Landscape;
            }

            public void Portrait()
            {
                ((Activity)Forms.Context).RequestedOrientation = ScreenOrientation.Portrait;
            }
        }
    }